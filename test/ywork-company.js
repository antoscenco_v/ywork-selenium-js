const webdriver = require('selenium-webdriver'),
    {describe, it, after, before, beforeEach, afterEach} = require('selenium-webdriver/testing'),
    By = webdriver.By,
    assert = require('assert'),
    until = webdriver.until;
const mlog = require('mocha-logger');
let find;

function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

describe('company scenario', function () {
    let driver;
    beforeEach(function () {
        driver = new webdriver.Builder().forBrowser('chrome').build();
        driver.manage().window().maximize();
        driver.get('https://13:13@ywork.dev.tekoway.com/');

    });


    afterEach(function () {
        driver.quit();
    });

    it('should login as company and create mission', function () {
        let randomTitle = makeid(5);
        driver.wait(
            until.elementLocated(By.xpath('/html/body/app-root/app-layout/mat-sidenav-container/mat-sidenav-content/main/app-home/div/div/div[1]/h1')),
            5000
        );

        let titleEl = driver.findElement(By.xpath('/html/body/app-root/app-layout/mat-sidenav-container/mat-sidenav-content/main/app-home/div/div/div[1]/h1'));
        titleEl.getText().then(function (value) {
            assert.equal(value, 'The Best Match  !');
        });

        driver.wait(
            until.elementsLocated(By.linkText('Me connecter')),
            5000
        );
        driver.findElement(By.xpath('/html/body/app-root/app-gdpr-bar/div/div/button'))
            .click();
        driver.findElement(By.linkText('Me connecter')).click();
        driver.findElement(By.xpath('//input[@formcontrolname=\'username\']'))
            .sendKeys('testing.ui.2018@gmail.com');
        driver.findElement(By.xpath('//input[@formcontrolname=\'password\']'))
            .sendKeys('Testtest1!');
        driver.findElement(By.xpath('//mat-checkbox[@formcontrolname=\'rememberMe\']'))
            .click();
        driver.findElement(By.xpath('//*[@id="mat-dialog-0"]/app-login/mat-dialog-actions/button'))
            .click();


        //missions link
        driver.wait(
            until.elementLocated(By.xpath('/html/body/app-root/app-layout/app-layout/mat-sidenav-container/mat-sidenav/div/div/app-menu/ul/li[3]/a/span[2]')),
            50000
        );
        driver.findElement(By.xpath('/html/body/app-root/app-layout/app-layout/mat-sidenav-container/mat-sidenav/div/div/app-menu/ul/li[3]/a/span[2]')).click();


        // create mission button
        driver.wait(
            until.elementLocated(By.xpath('/html/body/app-root/app-layout/app-layout/mat-sidenav-container/mat-sidenav-content/div/app-missions-recruitment/div/div/app-content-header/div/div[2]/button/span')),
            50000
        );
        driver.findElement(By.xpath('/html/body/app-root/app-layout/app-layout/mat-sidenav-container/mat-sidenav-content/div/app-missions-recruitment/div/div/app-content-header/div/div[2]/button/span')).click();

        driver.wait(
            until.elementLocated(By.xpath('//*[@id="sticky-actions"]/validation-bar/div/button[1]')),
            50000
        );

        //save mission button
        let saveButton = driver.findElement(By.xpath('//*[@id="sticky-actions"]/validation-bar/div/button[1]'));
        driver.wait(
            until.elementIsVisible(saveButton),
            50000
        );

        //title
        driver.findElement(By.xpath('//*[@id="mat-input-2"]')).sendKeys(randomTitle);
        driver.sleep(1000);

        //Sélectionner un secteur d'activité
        driver.findElement(By.xpath('/html/body/app-root/app-layout/app-layout/mat-sidenav-container/mat-sidenav-content/div/app-mission-page/div/div/form/div/div/div[3]/mat-form-field')).click();
        driver.wait(
            until.elementLocated(By.xpath('/html/body/div[3]/div[2]/div/div/div/mat-option[2]/span')),
            50000
        );

        driver.findElement(By.xpath('/html/body/div[3]/div[2]/div/div/div/mat-option[2]/span')).click();
        driver.sleep(1000);


        //Sélectionner une photo d'entête
        driver.findElement(By.xpath('/html/body/app-root/app-layout/app-layout/mat-sidenav-container/mat-sidenav-content/div/app-mission-page/div/div/form/div/div/div[5]/company-user-media-trigger/div/div')).click();
        driver.sleep(5000);

        driver.wait(
            until.elementLocated(By.xpath('/html/body/div[3]/div[2]/div')),
            50000
        );

        driver.wait(
            until.elementIsVisible(driver.findElement(By.xpath('/html/body/div[3]/div[2]/div'))),
            50000
        );

        driver.wait(
            until.elementLocated(By.xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/company-user-media-selector/app-media-selector/div[2]/div/form/div[2]/fieldset/div/img[1]')),
            50000
        );

        driver.wait(
            until.elementIsVisible(driver.findElement(By.xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/company-user-media-selector/app-media-selector/div[2]/div/form/div[2]/fieldset/div/img[1]'))),
            50000
        );
        driver.sleep(1000);
        driver.findElement(By.xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/company-user-media-selector/app-media-selector/div[2]/div/form/div[2]/fieldset/div/img[1]')).click();
        driver.sleep(1000);
        driver.findElement(By.xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/company-user-media-selector/app-media-selector/div[2]/div/form/mat-dialog-actions/button[2]/span')).click();
        driver.sleep(1000);

        //Sélectionner référent interne principal
        driver.findElement(By.id('mat-select-2')).click();
        driver.wait(
            until.elementLocated(By.xpath('/html/body/div[3]/div[2]/div/div/div/mat-option[2]/span')),
            50000
        );
        driver.sleep(1000);
        driver.findElement(By.xpath('/html/body/div[3]/div[2]/div/div/div/mat-option[2]/span')).click();
        driver.sleep(1000);

        //Nombre de poste(s)
        driver.findElement(By.id('mat-input-3')).sendKeys(1);

        //Volume d'intervention hebdomadaire souhaité

        driver.findElement(By.xpath('/html/body/app-root/app-layout/app-layout/mat-sidenav-container/mat-sidenav-content/div/app-mission-page/div/div/form/div/div/div[10]/mat-form-field/div/div[1]/div/mat-select')).click();
        driver.wait(
            until.elementLocated(By.xpath('/html/body/div[3]/div[2]/div/div/div/mat-option[2]/span')),
            50000
        );
        driver.sleep(1000);
        driver.findElement(By.xpath('/html/body/div[3]/div[2]/div/div/div/mat-option[2]/span')).click();
        driver.sleep(1000);


        //Selectionner un domaine d'expertise
        driver.findElement(By.xpath('/html/body/app-root/app-layout/app-layout/mat-sidenav-container/mat-sidenav-content/div/app-mission-page/div/div/form/div/div/div[12]/mat-form-field/div/div[1]/div/mat-select')).click();
        driver.wait(
            until.elementLocated(By.xpath('/html/body/div[3]/div[2]/div/div/div/mat-option[2]/span')),
            50000
        );
        driver.sleep(1000);
        driver.findElement(By.xpath('/html/body/div[3]/div[2]/div/div/div/mat-option[2]/span')).click();
        driver.sleep(1000);


        //Durée souhaitée de la mission
        let elDuree = driver.findElement(By.xpath('/html/body/app-root/app-layout/app-layout/mat-sidenav-container/mat-sidenav-content/div/app-mission-page/div/div/form/div/div/div[14]/mat-form-field/div/div[1]/div/mat-select/div/div[1]/span'));
        driver.executeScript("arguments[0].scrollIntoView()", elDuree);
        driver.sleep(300);
        elDuree.click();

        driver.wait(
            until.elementLocated(By.xpath('/html/body/div[3]/div[2]/div/div/div/mat-option[2]/span')),
            50000
        );
        driver.sleep(1000);
        driver.findElement(By.xpath('/html/body/div[3]/div[2]/div/div/div/mat-option[2]/span')).click();
        driver.sleep(1000);

        //Niveau d'expérience requis
        driver.findElement(By.xpath('/html/body/app-root/app-layout/app-layout/mat-sidenav-container/mat-sidenav-content/div/app-mission-page/div/div/form/div/div/div[15]/mat-form-field')).click();
        driver.wait(
            until.elementLocated(By.xpath('/html/body/div[3]/div/div/div/mat-option[1]/span')),
            50000
        );
        driver.sleep(1000);
        driver.findElement(By.xpath('/html/body/div[3]/div/div/div/mat-option[1]/span')).click();
        driver.sleep(1000);

        //Niveau d'études requis
        driver.findElement(By.xpath('/html/body/app-root/app-layout/app-layout/mat-sidenav-container/mat-sidenav-content/div/app-mission-page/div/div/form/div/div/div[16]/mat-form-field')).click();
        driver.wait(
            until.elementLocated(By.xpath('/html/body/div[3]/div/div/div/mat-option[2]/span')),
            50000
        );
        driver.sleep(1000);
        driver.findElement(By.xpath('/html/body/div[3]/div/div/div/mat-option[2]/span')).click();
        driver.sleep(1000);


        //Est-il essentiel que le talent travaille dans vos locaux?
        driver.findElement(By.xpath('/html/body/app-root/app-layout/app-layout/mat-sidenav-container/mat-sidenav-content/div/app-mission-page/div/div/form/div/div/div[17]/mat-form-field/div/div[1]/div/mat-select')).click();
        driver.wait(
            until.elementLocated(By.xpath('/html/body/div[3]/div[2]/div/div/div/mat-option[2]/span')),
            50000
        );
        driver.sleep(1000);
        driver.findElement(By.xpath('/html/body/div[3]/div[2]/div/div/div/mat-option[2]/span')).click();
        driver.sleep(1000);


        // Lieu de la mission
        driver.findElement(By.id('mat-input-9')).sendKeys('Paris');
        driver.wait(
            until.elementLocated(By.xpath('/html/body/div[3]/div/div/div/mat-option[1]/span')),
            50000
        );
        driver.sleep(1000);
        driver.findElement(By.xpath('/html/body/div[3]/div/div/div/mat-option[1]/span')).click();
        driver.sleep(1000);

        //description
        driver.findElement(By.id('mat-input-10')).sendKeys('description');
        //profil
        driver.findElement(By.id('mat-input-11')).sendKeys('description');


        //Savoir-faire requis
        let elSavoirRequis = driver.findElement(By.xpath('/html/body/app-root/app-layout/app-layout/mat-sidenav-container/mat-sidenav-content/div/app-mission-page/div/div/form/div/div/div[21]/mat-form-field/div/div[1]/div/mat-chip-list/div/input'));
        driver.executeScript("arguments[0].scrollIntoView()", elSavoirRequis);
        driver.sleep(300);
        elSavoirRequis.click();

        driver.wait(
            until.elementLocated(By.xpath('/html/body/div[3]/div/div/div/mat-option[2]/span')),
            50000
        );
        driver.sleep(1000);
        driver.findElement(By.xpath('/html/body/div[3]/div/div/div/mat-option[2]/span')).click();
        driver.sleep(1000);

        //Savoir-être souhaités
        driver.findElement(By.xpath('/html/body/app-root/app-layout/app-layout/mat-sidenav-container/mat-sidenav-content/div/app-mission-page/div/div/form/div/div/div[22]/mat-form-field/div/div[1]/div/mat-chip-list/div/input')).click();
        driver.wait(
            until.elementLocated(By.xpath('/html/body/div[3]/div/div/div/mat-option[2]/span')),
            50000
        );
        driver.sleep(1000);
        driver.findElement(By.xpath('/html/body/div[3]/div/div/div/mat-option[2]/span')).click();
        driver.sleep(1000);

        saveButton.click();

        driver.sleep(5000);

        let promise = driver.findElement(By.xpath("//*[contains(text(),'" + randomTitle + "')]")).getText();

        promise.then(function (text) {
            assert.equal(text, randomTitle);
        });
    });
});
