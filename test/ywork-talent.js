const webdriver = require('selenium-webdriver'),
    {describe, it, after, before, beforeEach, afterEach} = require('selenium-webdriver/testing'),
    By = webdriver.By,
    assert = require('assert'),
    until = webdriver.until;
const mlog = require('mocha-logger');
let find;

function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

describe('talent scenario', function () {
    let driver;
    beforeEach(function () {
        driver = new webdriver.Builder().forBrowser('chrome').build();
        driver.manage().window().maximize();
        driver.get('https://13:13@ywork.dev.tekoway.com/');

    });

    afterEach(function () {
        driver.quit();
    });

    it('should login as talent and update prestation', function () {
        let randomTitle = makeid(5);
        driver.wait(
            until.elementLocated(By.xpath('/html/body/app-root/app-layout/mat-sidenav-container/mat-sidenav-content/main/app-home/div/div/div[1]/h1')),
            5000
        );

        let titleEl = driver.findElement(By.xpath('/html/body/app-root/app-layout/mat-sidenav-container/mat-sidenav-content/main/app-home/div/div/div[1]/h1'));
        titleEl.getText().then(function (value) {
            assert.equal(value, 'The Best Match  !');
        });

        driver.wait(
            until.elementsLocated(By.linkText('Me connecter')),
            5000
        );
        driver.findElement(By.xpath('/html/body/app-root/app-gdpr-bar/div/div/button'))
            .click();
        driver.findElement(By.linkText('Me connecter')).click();
        driver.findElement(By.xpath('//input[@formcontrolname=\'username\']'))
            .sendKeys('talent1@gmail.com');
        driver.findElement(By.xpath('//input[@formcontrolname=\'password\']'))
            .sendKeys('Testtest1!');
        driver.findElement(By.xpath('//mat-checkbox[@formcontrolname=\'rememberMe\']'))
            .click();
        driver.findElement(By.xpath('//*[@id="mat-dialog-0"]/app-login/mat-dialog-actions/button'))
            .click();
        driver.wait(
            until.elementLocated(By.xpath('/html/body/app-root/app-layout/talent-layout/mat-sidenav-container/mat-sidenav/div/div/app-menu/ul/li[2]/a/span[2]')),
            5000
        );
        driver.findElement(By.xpath('/html/body/app-root/app-layout/talent-layout/mat-sidenav-container/mat-sidenav/div/div/app-menu/ul/li[2]/a/span[2]')).click();

        driver.wait(
            until.elementLocated(By.css('#sticky-actions > validation-bar > div > button')),
            5000
        );
        driver.wait(
            until.elementLocated(By.css('#mat-input-12')),
            5000
        );

        let prestationEl = driver.findElement(By.css('#mat-input-12'));

        prestationEl.clear();
        prestationEl.sendKeys(randomTitle);
        driver.findElement(By.css('#sticky-actions > validation-bar > div > button')).click();

    });
});
